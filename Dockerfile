# antiSMASH container with a snapshot of the development tree
# VERSION 0.0.3
FROM antismash/base-nonfree:latest
LABEL maintainer="Kai Blin <kblin@biosustain.dtu.dk>"

ENV ANTISMASH_VERSION="dev"

# Install git
RUN apt-get update && apt-get install -y git && apt-get clean -y && apt-get autoclean -y && apt-get autoremove -y && rm -rf /var/lib/apt/lists/*

# Grab antiSMASH
COPY . /antismash-${ANTISMASH_VERSION}

ADD docker/instance.cfg /antismash-${ANTISMASH_VERSION}/antismash/config/instance.cfg

# Create the shared bgc_seeds.hmm file
RUN /antismash-${ANTISMASH_VERSION}/scripts/create_bgc_seeds.sh /antismash-${ANTISMASH_VERSION}

# compress the shipped profiles
RUN hmmpress /antismash-${ANTISMASH_VERSION}/antismash/specific_modules/nrpspks/abmotifs.hmm && \
    hmmpress /antismash-${ANTISMASH_VERSION}/antismash/specific_modules/nrpspks/dockingdomains.hmm && \
    hmmpress /antismash-${ANTISMASH_VERSION}/antismash/specific_modules/nrpspks/ksdomains.hmm && \
    hmmpress /antismash-${ANTISMASH_VERSION}/antismash/specific_modules/nrpspks/nrpspksdomains.hmm && \
    hmmpress /antismash-${ANTISMASH_VERSION}/antismash/specific_modules/nrpspks/sandpuma/flat/fullset0_smiles/fullset0_smiles_nrpsA.hmmdb && \
    hmmpress /antismash-${ANTISMASH_VERSION}/antismash/generic_modules/smcogs/smcogs.hmm && \
    hmmpress /antismash-${ANTISMASH_VERSION}/antismash/generic_modules/active_site_finder/hmm/p450.hmm3 && \
    hmmpress /antismash-${ANTISMASH_VERSION}/antismash/generic_modules/hmm_detection/bgc_seeds.hmm

ADD docker/run /usr/local/bin/run

WORKDIR /usr/local/bin
RUN ln -s /antismash-${ANTISMASH_VERSION}/run_antismash.py

RUN mkdir /matplotlib && MPLCONFIGDIR=/matplotlib python -c "import matplotlib.pyplot as plt" && chmod -R a+rw /matplotlib

VOLUME ["/input", "/output", "/databases"]
WORKDIR /output

ENTRYPOINT ["/usr/local/bin/run"]
