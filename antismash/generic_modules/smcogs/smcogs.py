# vim: set fileencoding=utf-8 :
#
# Copyright (C) 2010-2012 Marnix H. Medema
# University of Groningen
# Department of Microbial Physiology / Groningen Bioinformatics Centre
#
# Copyright (C) 2011,2012 Kai Blin
# University of Tuebingen
# Interfaculty Institute of Microbiology and Infection Medicine
# Div. of Microbiology/Biotechnology
#
# License: GNU Affero General Public License v3 or later
# A copy of GNU AGPL v3 should have been included in this software package in LICENSE.txt.

import sys
import os
import shutil
import subprocess
import time
import logging
import signal
from cStringIO import StringIO
from antismash import utils


def smcog_analysis(inputgenes, inputnr, seq_record, smcogdict, smcogsoutputfolder):
    "run smCOG search on all gene cluster CDS features"
    for feature in inputgenes:
        k = utils.get_gene_id(feature)
        tag = k
        seq = str(utils.get_aa_sequence(feature))
        # create input.fasta file with single query sequence to be used as input for MSA
        utils.writefasta([tag], [seq], "input" + str(inputnr) + ".fasta")
        if smcogdict.has_key(k) and len(smcogdict[k]) > 0:
            smcog = (smcogdict[k][0][0]).split(":")[0]
            if not alignsmcogs(smcog, inputnr):
                continue
            # Generate trimmed alignment
            trimalignment(inputnr)
            # Draw phylogenetic tree
            drawtree(inputnr, smcogsoutputfolder, tag)


def alignsmcogs(smcog, inputnr):
    # Align to multiple sequence alignment, output as fasta file
    infile1 = utils.get_full_path(__file__, "%s_muscle.fasta" % str(smcog).lower())
    musclecommand = ["muscle", "-quiet", "-profile", "-in1", infile1, "-in2", "input" + str(inputnr) + ".fasta", "-out", "muscle" + str(inputnr) + ".fasta"]
    _, err, retcode = utils.execute(musclecommand)
    if retcode != 0:
        # TODO: Figure out why this might happen, and then treat it as a proper error
        logging.debug("smCOG muscle command failed: %d %r", retcode, err)
        return False
    return True


def trimalignment(inputnr):
    #Trim alignment
    #edit muscle fasta file: remove all positions before the first and after the last position shared by >33% of all sequences
    musclefile = open("muscle" + str(inputnr) + ".fasta","r")
    filetext = musclefile.read()
    filetext = filetext.replace("\r","\n")
    lines = filetext.split("\n")
    ##Combine all sequence lines into single lines
    lines2 = []
    seq = ""
    nrlines = len(lines)
    a = 0
    lines = lines[:-1]
    for i in lines:
        if a == (nrlines - 2):
            seq = seq + i
            lines2.append(seq)
        if i[0] == ">":
            lines2.append(seq)
            seq = ""
            lines2.append(i)
        else:
            seq = seq + i
        a += 1
    lines = lines2[1:]
    #Retrieve names and seqs from muscle fasta lines
    seqs = []
    names = []
    for i in lines:
        if len(i) > 0 and i[0] == ">":
            name = i[1:]
            names.append(name)
        else:
            seq = i
            seqs.append(seq)
    #Find first and last amino acids shared conserved >33%
    #Create list system to store conservation of residues
    conservationlist = []
    lenseqs = len(seqs[0])
    nrseqs = len(seqs)
    for i in range(lenseqs):
        conservationlist.append({"A":0,"B":0,"C":0,"D":0,"E":0,"F":0,"G":0,"H":0,"I":0,"J":0,"K":0,"L":0,"M":0,"N":0,"P":0,"Q":0,"R":0,"S":0,"T":0,"U":0,"V":0,"W":0,"X":0,"Y":0,"Z":0,"-":0})
    a = 0
    for i in seqs:
        aa = list(i)
        for i in aa:
            conservationlist[a][i] += 1
            a += 1
        a = 0
    firstsharedaa = 0
    lastsharedaa = lenseqs
    #Find first amino acid shared
    first = "yes"
    nr = 0
    for i in conservationlist:
        aa = utils.sortdictkeysbyvaluesrev(i)
        if aa[0] != "-" and i[aa[0]] > (nrseqs / 3) and first == "yes":
            firstsharedaa = nr
            first = "no"
        nr += 1
    #Find last amino acid shared
    conservationlist.reverse()
    first = "yes"
    nr = 0
    for i in conservationlist:
        aa = utils.sortdictkeysbyvaluesrev(i)
        if aa[0] != "-" and i[aa[0]] > (nrseqs / 3) and first == "yes":
            lastsharedaa = lenseqs - nr
            first = "no"
        nr += 1
    #Shorten sequences to detected conserved regions
    seqs2 = []
    for i in seqs:
        seq = i[firstsharedaa:lastsharedaa]
        seqs2.append(seq)
    seqs = seqs2
    seedfastaname = "trimmed_alignment" + str(inputnr) + ".fasta"
    utils.writefasta(names, seqs, seedfastaname)


def drawtree(inputnr, smcogsoutputfolder, tag):
    import matplotlib
    matplotlib.use('Agg')
    import pylab
    from Bio import Phylo
    from Bio.Phylo.NewickIO import NewickError

    # Cacluclate phylogenetic tree with fasttree
    fasttreecommand = ["fasttree", "-quiet", "-fastest", "-noml", "trimmed_alignment" + str(inputnr) + ".fasta"]
    out, err, ret = utils.execute(fasttreecommand)
    if ret != 0:
        logging.debug("fasttree failed with retcode %d: %r", ret, err)
        return

    # And draw it with the matplotlib/pylab/Bio.Phylo
    handle = StringIO(out)
    try:
        tree = Phylo.read(handle, 'newick')
        Phylo.draw(tree, do_show=False)
        fig = matplotlib.pyplot.gcf()
        fig.set_size_inches(20, (tree.count_terminals() / 3))
        matplotlib.pyplot.axis('off')
        pylab.savefig(os.path.join(smcogsoutputfolder, tag + '.png'), bbox_inches='tight')
        matplotlib.pyplot.close(fig)
    except NewickError:
        logging.debug('Invalid newick tree for %r', tag)
        return
