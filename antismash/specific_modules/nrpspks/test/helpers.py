# License: GNU Affero General Public License v3 or later
# A copy of GNU AGPL v3 should have been included in this software package in LICENSE.txt.

from __future__ import print_function, division

import tempfile
import unittest

class NRPSAnalysisTestBase(unittest.TestCase):
    """ For testing the set of common NRPS classification functions that
        take two args, a fasta filename for input and a filename for output.
        The data in the fasta file should be provided as query_data.
    """

    def run_with_tempfile(self, function, result_path, query_data):
        with tempfile.NamedTemporaryFile() as datafile:
            for k, v in query_data.items():
                datafile.write(">{}\n{}\n".format(k, v))
            datafile.flush()
            with open(result_path) as expected:
                with tempfile.NamedTemporaryFile() as temp:
                    function(datafile.name, temp.name)
                    self.assertEqual(expected.readlines(), temp.readlines())

