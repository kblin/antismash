# License: GNU Affero General Public License v3 or later
# A copy of GNU AGPL v3 should have been included in this software package in LICENSE.txt.

from __future__ import print_function, division

import logging
from os import path

from antismash import utils

def hmmsearch(lsname, fasta, hmm):
    # type: (str, str, str) -> Dict[str, str]
    text, err, exitcode = utils.execute(["hmmsearch", "--noali", hmm, fasta])
    if exitcode:
        logging.error("hmmsearch stderr: %s", err)
        raise RuntimeError("hmmsearch exited non-zero")
    if "[No targets detected" in text:
        return {lsname : str(0)}
    text = text.replace("\r","\n")
    start = text.find('Domain annotation for each sequence:')
    end = text.find('Internal pipeline statistics summary:')
    ls_scores = []
    lines = text[start:end].split('\n')
    for line in lines[4:-4]:
        tabs = [i for i in line.split(" ") if i]
        ls_scores.append(tabs[2])
        break
    return {lsname : ls_scores[0]}

def get_positions(filename, startpos):
    # type: (str, int) -> List[int]
    with open(filename, "r") as f:
        text = f.read().strip().replace(' ', '_')
    return [int(i) - startpos for i in text.split("\t")]

def get_poslist(sequence, positions):
    # type: (str, List[int]) -> List[int]
    poslist = []
    c = 0
    for pos, element in enumerate(sequence):
        if element == "-":
            continue
        if c in positions:
            poslist.append(pos)
        c += 1
    return poslist

def run_minowa(query_file, startpos, outfile, muscle_ref, ref_sequence,
               positions_file, data_dir, hmm_names):
    # type: (str, int, str, str, str, str, str, List[str]) -> None
    out_file = open(outfile,"w")
    sequence_info = utils.read_fasta(query_file)
    positions = get_positions(positions_file, startpos)

    for query_id, query_seq in sequence_info.items():
        utils.writefasta([query_id], [query_seq], "infile.fasta")

        #Run muscle and collect sequence positions from file
        utils.execute(["muscle", "-profile", "-quiet", "-in1", muscle_ref,
                       "-in2", "infile.fasta", "-out", "muscle.fasta"])
        muscle = utils.read_fasta("muscle.fasta")

        #Count residues in ref sequence and put positions in list
        poslist = get_poslist(muscle[ref_sequence], positions)

        #Extract positions from query sequence and create fasta file to use as input for hmm searches
        query = muscle[query_id]
        seq = "".join([query[position].replace("-","X") for position in poslist])
        utils.writefasta([query_id], [seq], "hmm_infile.fasta")

        #- then use list to extract positions from every sequence -> HMMs (one time, without any query sequence)
        hmm_scores = {}
        for hmmname in hmm_names:
            hmmresults = hmmsearch(query_id, "hmm_infile.fasta", path.join(data_dir, hmmname + ".hmm"))
            hmm_scores[hmmname] = float(hmmresults.values()[0])

        results = sorted(hmm_scores.items(), reverse=True, key=lambda x: (x[1], x[0]))

        out_file.write("\\\\\n" + query_id + "\n")
        out_file.write("Substrate:\tScore:\n")
        for name, score in results:
            out_file.write("{}\t{}\n".format(name, score))

    out_file.close()

