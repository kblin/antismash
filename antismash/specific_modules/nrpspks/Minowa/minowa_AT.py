# License: GNU Affero General Public License v3 or later
# A copy of GNU AGPL v3 should have been included in this software package in LICENSE.txt.

from __future__ import print_function, division

from antismash import utils
from antismash.specific_modules.nrpspks.Minowa.base import run_minowa

def run_minowa_at(infile, outfile):
    run_minowa(query_file = infile,
               startpos = 7,
               outfile = outfile,
               muscle_ref = utils.get_full_path(__file__, "AT_domains_muscle.fasta"),
               ref_sequence = "P0AAI9_AT1",
               positions_file = utils.get_full_path(__file__, "ATpositions.txt"),
               data_dir = utils.get_full_path(__file__, 'AT_HMMs'),
               hmm_names = ["2-Methylbutyryl-CoA",
                            "Acetyl-CoA",
                            "CHC-CoA",
                            "fatty_acid",
                            "Isobutyryl-CoA",
                            "Methoxymalonyl-CoA",
                            "Propionyl-CoA",
                            "3-Methylbutyryl-CoA",
                            "Benzoyl-CoA",
                            "Ethylmalonyl-CoA",
                            "inactive",
                            "Malonyl-CoA",
                            "Methylmalonyl-CoA",
                            "trans-1,2-CPDA"])

