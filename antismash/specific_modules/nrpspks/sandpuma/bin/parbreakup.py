# set fileencoding: utf-8
# License: GNU Affero General Public License v3 or later
# A copy of GNU AGPL v3 should have been included in this software package in LICENSE.txt.

from __future__ import print_function, division

import os
import shutil
import logging

from antismash import utils
from ..bin import par

def write_multiple_fastas(sequence_info, number_of_files, file_prefix="partition"):
    # type: (Dict[str, str], int, str) -> List[str]
    chunk = len(sequence_info)
    if number_of_files != 1:
        chunk = chunk // number_of_files
        if chunk * number_of_files < len(sequence_info):
            chunk += 1
        logging.debug("Running %d parallel sections, each with a max of %d A domains", 
                min(number_of_files, len(sequence_info)), chunk)
    else:
        logging.debug("Processing %d A domains", len(sequence_info))
    files = []
    items = sorted(sequence_info.items())
    keys, sequences = zip(*items)
    i = 0
    while i*chunk < len(keys):
        filename = "{}.{}.faa".format(file_prefix, i)
        files.append(filename)
        start = i * chunk
        end = start + chunk
        utils.writefasta(keys[start:end], sequences[start:end], filename)
        i += 1
    return files

def main(sequence_info, threads, pref="input_adomains"):
    # type: (Dict[str, str], int, str) -> None
    files = write_multiple_fastas(sequence_info, threads, pref)

    par.run_parallel_fasttree(files)

    result_files = ('ind.res.tsv', 'ens.res.tsv', 'pid.res.tsv', 'query.rep')
    # remove the old if they exist
    for filename in result_files:
        try:
            if os.path.exists(filename):
                os.remove(filename)
        except OSError as e:
            raise OSError("Could not remove old data %s: %s", filename, str(e))

    target_files = [open(filename, "w") for filename in result_files]

    for section in files:
        child_dir = section.rsplit(".faa", 1)[0]
        for target, name in zip(target_files, result_files):
            with open(os.path.join(child_dir, name), "r") as child_results:
                target.writelines(child_results.readlines())
        try:
            shutil.rmtree(child_dir)
        except OSError:
            logging.error("failed to remove temp directories in parbreakup")

    for target in target_files:
        target.close()

