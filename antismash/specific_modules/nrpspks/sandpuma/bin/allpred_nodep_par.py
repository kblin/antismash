# License: GNU Affero General Public License v3 or later
# A copy of GNU AGPL v3 should have been included in this software package in LICENSE.txt.

from __future__ import print_function, division, absolute_import

import logging
import os
import shutil
import sys
import tempfile
from collections import defaultdict

from antismash import utils
from ..bin import treeparserscore
from ..bin import classifyp
from ..bin import nrpscodepred

class ValueContainer(object):
    """ Helps by separating data values into more meaningful sections
        Only intended for use in Ensembler
    """
    __slots__ = ["values", "methods"]
    def __init__(self, values):
        self.values = values
        self.methods = defaultdict(lambda: "no_call")

class Ensembler(object):
    """ Holds data for ensemble process so it isn't reconstructed every use """
    data_file = None # type: str
    features = [] # type: List[List[float]]
    labels = [] # type: List[str]
    specificity_loc = {} # type: Dict[str, int]
    specificities = [] # type: List[str]
    data = {} # type: Dict[str, ValueContainer]

    def __init__(self, data_file, max_depth, min_leaf_support):
        # type: (str, int, int) -> None
        self.query_mapping = {}  # type: Dict[str, str] # from full query name to spec
        if self.data_file is None or data_file != self.data_file:
            self.construct_specmap(data_file)
            self.construct_features_and_labels()
            self.data_file = data_file
        self.max_depth = max_depth
        self.min_leaf_support = min_leaf_support

    def construct_features_and_labels(self):
        # type: () -> None
        """ Build the feature and label lists from self.data """
        self.features = []
        self.labels = []
        methods = ['prediCAT', 'forced_prediCAT_snn50', 'svm', 'stach', 'pHMM']
        for uname in sorted(self.data.keys()):
            values = self.data[uname].values
            self.labels.append(str(self.specificity_loc[values["true"]]))
            self.features.append([float(values["pid"])])
            d = self.data[uname].methods
            for method in methods:
                self.features[-1].extend(self.getmatrix(d[method]))

    def construct_specmap(self, data_file):
        # type: (str) -> None
        """ Reads in the data and stores it """
        self.data = {}  # type: Dict[str, ValueContainer]
        self.specificities = []  # type: List[str]
        self.specificity_loc = {}  # maps spec name -> location in specifities # type: Dict[str, int]
        data = open(data_file, "r")
        for line in data:
            line = line.strip()
            if line.startswith("shuffle"):
                continue
            shuf, jk, query, pid, truespec, called_spec, method, covered, _, _, uname, b = line.split('\t')
            if uname not in self.data:
                self.data[uname] = ValueContainer({'true':truespec, 'pid':pid,
                                                   'shuf':shuf, 'jk':jk,
                                                   'query':query, 'bin':b})
            if covered == "N":
                called_spec = "no_call"
            self.data[uname].methods[method] = called_spec
            self.specificity_loc[truespec] = -1
            self.specificity_loc[called_spec] = -1
            new_query = query.replace("(", "-").replace(")", "-")
            self.query_mapping[new_query] = new_query.rsplit("_", 1)[1]
        data.close()
        # keep an ordered set of specificities
        self.specificities = sorted(self.specificity_loc.keys())
        # and mark each location as having a location
        for n, spec in enumerate(self.specificities):
            self.specificity_loc[spec] = n

    def getmatrix(self, spec):
        # type: (str) -> List[int]
        """ Builds a list for use in decision trees """
        # we could use scipy.csr_matrix instead since it's so sparse, but
        # that will require a better assembly of all components
        mat = [0]*len(self.specificities)
        if spec in self.specificity_loc:
            mat[self.specificity_loc[spec]] = 1
        return mat

    def run(self, pc, pcf, asm, svm, phmm, pid):
        # type: (str, str, str, str, str, str) -> str
        matrix = [float(pid)]
        pc_spec = self.query_mapping.get(pc, "no_call")
        if pcf == "no_force_needed":
            pcf = pc_spec
        elif pcf == "no_confident_result":
            pcf = "no_call"
        for val in [pc_spec, pcf, svm, asm, phmm]:
            matrix.extend(self.getmatrix(val))

        classification = classifyp.main(matrix, self.max_depth, self.min_leaf_support,
                                        self.features, self.labels)
        retval = self.specificities[int(classification)]
        return retval


# we know input is overriding a default, but it has to match the existing signature
# pylint: disable=redefined-builtin
def execute(cmd, input=None, outfile=None, empty_out=False):
    # type: (List[str], str, str, bool) -> str
    """ Wraps utils.execute because this file uses so very many external
        programs

        cmd: the list of args to execute
        input: single string input to pipe in
        outfile: file to pipe output to
        empty_out: ok to not have output

        returns stdout as single string or
            True if one of ``outfile`` or ``empty_out`` specified
    """
    if outfile is not None:
        handle = open(outfile, "w")
        out, err, ex = utils.execute(cmd, input, stdout=handle)
        handle.close()
        out = True
    else:
        out, err, ex = utils.execute(cmd, input)

    if ex:
        raise RuntimeError("%s exited with non-zero value, stderr tail:%s" %(cmd[0], err[-100:]))
    if not empty_out and not out:
        raise RuntimeError("%s failed to generate output, stderr tail:%s" %(cmd[0], err[-100:]))
    return out
# pylint: enable=redefined-builtin

def read_fasta(input_lines):
    # type: (List[str]) -> Dict[str, str]
    """ converts a list of fasta-like lines into a dict: id -> sequence
        returns the dict """
    ids = []
    sequence_info = [] # type: List[str]

    for line in input_lines:
        line = line.strip()
        if not line:
            continue
        if line[0] == '>':
            ids.append(line[1:])
        else:
            if not ids:
                raise ValueError("Found sequence without identifier")
            if len(sequence_info) == len(ids):
                sequence_info[-1] += line
            else:
                sequence_info.append(line)
    if len(ids) != len(sequence_info):
        raise ValueError("sequence identifier and sequence info mismatch")
    return dict(zip(ids, sequence_info))

def main(filename):
    # type: (str) -> None
    ## Get base dir
    basedir = os.path.dirname(os.path.dirname(__file__))

    ## Global Setting(s)
    wildcard = 'UNK' # other files have this hardcoded also

    ## PrediCAT Setting(s)
    knownfaa = os.path.join(basedir, "flat", "fullset0_smiles.faa")
    snnthresh = 0.5 ## Default: 0.5

    ## ASM Setting(s)
    knownasm = os.path.join(basedir, "flat", "fullset0_smiles.stach.faa")

    ensembler = Ensembler(os.path.join(basedir, "flat", "alldata.tsv"),
                          max_depth=40, min_leaf_support=10)

    dirpref = os.path.dirname(filename)
    os.chdir(dirpref)
    queries = utils.read_fasta(os.path.basename(dirpref)+".faa")

    ## Run predictors
    independent_results = []
    ensemble_results = []
    pid_results = []
    for seq_id in queries:
        query = ("{}_{}".format(seq_id, wildcard), queries[seq_id])
        ## Run prediCAT
        pc, pcf, snnscore = treescanner(knownfaa, query, wildcard)
        independent_results.append("\t".join([seq_id, "prediCAT_MP", pc]) + "\n")
        if snnscore != "N/A" and float(snnscore) < snnthresh:
            pcf = 'no_confident_result'
        independent_results.append("\t".join([seq_id, "prediCAT_SNN", pcf, snnscore]) + "\n")
        ## Run ASM
        asm = code2spec(knownasm, stachextract(query, basedir))
        independent_results.append("\t".join([seq_id, 'ASM', asm]) + "\n")
        ## Run SVM
        svm = process_svm(query, basedir)
        independent_results.append("\t".join([seq_id, 'SVM', svm]) + "\n")
        ## Run pHMM
        phmm = processphmm(knownfaa, query)
        independent_results.append("\t".join([seq_id, 'pHMM', phmm]) + "\n")
        # get pid from blastp
        pid = getpid(query, knownfaa)
        pid_results.append("{}\t{}\n".format(seq_id, pid))
        ## Run Ensemble
        ens = ensembler.run(pc, pcf, asm, svm, phmm, pid)
        ensemble_results.append("\t".join([seq_id, 'ENS', ens]) + "\n")

    # write results to file
    with open("ind.res.tsv", "w") as result_file:
        result_file.writelines(independent_results)
    with open("ens.res.tsv", "w") as result_file:
        result_file.writelines(ensemble_results)
    with open("pid.res.tsv", "w") as result_file:
        result_file.writelines(pid_results)

def getfirstseq(filename):
    # type: (str) -> Tuple[str, str]
    """ Returns the first sequence in a fasta file,
        in the format: tuple(id, sequence) """
    seqid = None
    seq = []
    with open(filename, "r") as data:
        for line in data:
            line = line.strip()
            if line[0] == '>':
                if seqid:
                    break
                seqid = line[1:]
            else:
                seq.append(line)
    return seqid, "".join(seq)

def concatenate_file(source, target):
    # type: (Union[BinaryIO, str], Union[BinaryIO, str]) -> None
    """ appends the contents of 'source' to 'target'
        types may be strings or file handles """
    if type(source) == str:
        source = open(source, "rb")
    if type(target) == str:
        target = open(target, "ab")
    if not getattr(source, "read") or not getattr(target, "write"):
        raise TypeError("can only concatenate if given file handles or"
                        "filenames (types %s and %s)"%(type(source), type(target)))
    shutil.copyfileobj(source, target)

def fasttree(input_filename):
    # type: (str) -> str
    out = execute(["mafft", "--quiet", "--namelength", "60", input_filename])
    #exception should be raised in the execute wrapper, but to be sure...
    assert out is not None
    out = execute(["fasttree", "-quiet"], input=out)
    assert out is not None
    return out

def treescanner(basefaa, query, wildcard):
    # type: (str, Tuple[str, str], str) -> Tuple[str, str, str]
    def make_fasttree_safe(line):
        # replace "(" and ")" with "-", otherwise parsing the tree will break
        return line.replace("(", "-").replace(")", "-")
    query_id, query_seq = query
    with open("tmp.trim.faa", "w") as mafft_input:
        mafft_input.write(">%s\n%s\n" % getfirstseq(basefaa))
        mafft_input.write(">{}\n{}\n".format(query_id, query_seq))
    ## Align query to first seq
    execute(["mafft", "--quiet", "--namelength", "60", "--op", "5", "tmp.trim.faa"],
            outfile="tmp.trim.afa")
    alignments = utils.read_fasta('tmp.trim.afa')
    aligned_queries = {}
    for seq_id, sequence in alignments.items():
        if seq_id.endswith(wildcard):
            aligned_queries[seq_id] = sequence.replace("-", "")
    if len(aligned_queries) != 1:
        raise ValueError("mafft lost track of a sequence")
    ## Compile all seqs
    filename = "tmp.tree.faa"
    with open(filename, "w") as compilation:
        # add the query
        for seq_id, sequence in aligned_queries.items():
            compilation.write('>{}\n{}\n'.format(make_fasttree_safe(seq_id), sequence))
        # add the reference set
        with open(basefaa, "r") as predicat_base:
            for line in predicat_base.readlines():
                compilation.write(make_fasttree_safe(line))
    ## Align all seqs, make tree
    results = get_tree_score(fasttree(filename))
    if not results:
        results = ("N/A", "no_confident_result", "N/A")
    return results

## prediCAT predictor
def get_tree_score(tree):
    # type: (str) -> Tuple[str, str, str]
    result = treeparserscore.main(tree)[0]
    return (result[2], result[3], result[6]) # hit, force, snnscore

## Extract codes for the ASM
def stachextract(query_sequence, basedir):
    # type: (Tuple[str, str], str) -> str
    gapopen = 3.4
    seed = basedir + "/flat/seed.afa"
    if not os.path.exists(seed):
        raise OSError("No such file: %s" % seed)

    ## Make alignment to example stachelhaus seeds
    with open("squery.faa", "w") as query_file:
        query_file.write(">{}\n{}\n".format(query_sequence[0], query_sequence[1]))
        concatenate_file(seed, query_file)
    out = execute(["mafft", "--quiet", "--op", str(gapopen),
                   "--namelength", "40", "squery.faa"])
    os.remove('squery.faa')
    ## Loop through alignment and get start and end sites based on guide seq
    aligned = read_fasta(out.split("\n"))
    start_point = None
    end_point = None
    guide_sequence = aligned["phe_grsA"]
    position = 0
    for i in range(1, len(guide_sequence) + 1):
        if guide_sequence[i-1].isalnum() or guide_sequence[i-1] == '_':
            position += 1
        if position == 1: # start of stach section
            start_point = i
        elif position == 106: # end of stach section
            end_point = i
    if not start_point or not end_point:
        logging.error("Issue with finding start/end seqs in sequence %s."
                      " Check MSA and consider higher gap open penalty.",
                      query_sequence[0])
        return None
    ## trim
    trimmed = {}
    positions = []
    for aligned_id, aligned_seq in aligned.items():
        trimmed[aligned_id] = aligned_seq[start_point-1:end_point+1]
    ## Extract stachelhaus positions from the alignment
    guide_sequence = trimmed["phe_grsA"]
    ## Indicies for stachelhaus code in grsA WITHOUT gaps
    # these are the 9 amino acids that stachelhaus cares about
    guide_positions = set([4, 5, 8, 47, 68, 70, 91, 99, 100])
    position = 0
    for i in range(1, len(guide_sequence)):
        if guide_sequence[i-1].isalnum() or guide_sequence[i-1] == '_':
            position += 1
        if position in guide_positions:
            positions.append(i-1)

    ## Verify the reference sequences
    check = {'phe_grsA':'DAWTIAAIC', 'asp_stfA-B2':'DLTKVGHIG',
             'orn_grsB3': 'DVGEIGSID', 'val_cssA9':'DAWMFAAVL'}
    result_code = None
    for seq_id, sub in trimmed.items():
        stachelhaus_code = "".join([sub[p] for p in positions])
        if seq_id == query_sequence[0]:
            result_code = stachelhaus_code
            continue
        if check[seq_id] != stachelhaus_code:
            msg = "Positive cntrl Stach codes do not match for %s expected %s, got %s"
            logging.error(msg, seq_id, check[seq_id], stachelhaus_code)
            return None
    return result_code

## Assign ASM code specificities
def code2spec(trainstach, query_code):
    # type: (str, str) -> str
    """ Predicts the specificity of a stachelhaus code (without the trailing K).
        The query_code must match at least 7 of 9 places to a training set code
        for a spec to be predicted.
        Returns a |-separated string of predictions.
    """

    def build_predictions(subkeys):
        # type: (Iterable[str]) -> Set[str]
        predictions = set()
        for subkey in subkeys:
            predictions.update(set(subkey.split("|")))
        return predictions

    if query_code is None or not query_code:
        logging.warning("ASM query empty")
        return "no_call"

    if type(query_code) != str:
        raise TypeError("Invalid ASM query code type")
    assert len(query_code) == 9

    # grab the reference set of codes -> types
    results = defaultdict(set) # type: DefaultDict[str, Set[str]]
    training_set = utils.read_fasta(trainstach)
    for seq_id, code in training_set.items():
        if '-' in code:
            continue
        subkey = seq_id.rsplit('_')[-1] # "thing_ser|dpg" -> "ser|dpg"
        results[code].add(subkey)

    # compare the query to the reference types
    if results.get(query_code): # direct hit
        predictions = build_predictions(results[query_code])
    else: # find the closest match
        high_score = 0
        scoring_keys = [] # type: List[str]
        for key in results.keys():
            correct = 0
            for a, b in zip(query_code, key):
                if a == b:
                    correct += 1
            if correct > high_score:
                scoring_keys = []
                high_score = correct
            if correct == high_score:
                scoring_keys.append(key)
        if high_score < 7:
            return "no_call"
        predictions = set()
        for key in scoring_keys:
            predictions.update(build_predictions(results[key]))
    return "|".join(sorted(list(predictions)))

## Predict on SVM Models
def process_svm(query, basedir):
    # type: (Tuple[str, str], str) -> str
    ## Make sure models are current
    target = basedir + "/dependencies/NRPSPredictor2/data/models/NRPS2_SINGLE_CLUSTER/[gly].mdl"
    if not os.path.exists(target):
        target_dir = basedir + "/dependencies/NRPSPredictor2/data/models/NRPS2_SINGLE_CLUSTER"
        if os.path.isdir(target_dir):
            shutil.rmtree(target_dir)
        shutil.copytree(basedir + "/flat/svm/NRPS2_SINGLE_CLUSTER", target_dir)

    ## Set up environment
    nrps2 = basedir + "/dependencies/NRPSPredictor2/NRPSpredictor2.sh"
    ## Get signatures
    nrps_input_file = tempfile.NamedTemporaryFile(prefix="antismash_nrps")
    nrps_output_file = "query.rep" #expected in parbreakup.py for outputdir
    nrpscodepred.run_nrpscodepred(query, nrps_input_file.name)
    ## Run SVMs
    execute([nrps2, basedir, "-i", nrps_input_file.name,
             "-r", nrps_output_file, "-s", "1"], outfile="/dev/null")
    nrps_input_file.close()
    ## Get results
    rep = open(nrps_output_file, "r")
    for line in rep:
        if line[0] == '#':
            continue
        res = line.strip().split('\t')
        break
    rep.close()
    svmpred = res[6]
    return svmpred

def getpid(query, knownfaa):
    # type: (Tuple[str, str], str) -> str
    db = knownfaa.rsplit('.faa', 1)[0] + ".db"

    if not os.path.exists(db  + ".pin"):
        raise RuntimeError("required blastdb missing: %s", db + ".pin")
        #TODO: this line should moved out of the parallel run into dep checks
        utils.execute(["makeblastdb", "-in", knownfaa, "-out", db, "-dbtype", "prot"])
    out = execute(["blastp", "-db", db, "-outfmt", "6 tabular, pident",
                   "-evalue", "1e-10", "-num_threads", "1"],
                  input=">{}\n{}\n".format(query[0], query[1]),
                  empty_out=True)
    if not out:
        return "0"
    pid = out.split("\n", 1)[0].strip()
    return pid

def processphmm(trainfaa, query):
    # type: (str, Tuple[str, str]) -> str
    path, filename = trainfaa.rsplit("/", 1)
    filename = filename.rsplit(".faa", 1)[0] + "_nrpsA.hmmdb"
    path = os.path.join(path, "fullset0_smiles")
    hmmdb = os.path.join(path, filename)
    # convert query to file for use with external programs
    query_filename = None
    # not using NamedTempFile because it has to exist on the filesystem without
    # an open write handle
    out = None
    try:
        query_fd, query_filename = tempfile.mkstemp(prefix="antismash")
        os.close(query_fd)
        open(query_filename, "w").write(">{}\n{}\n".format(query[0], query[1]))
        out = execute(["hmmscan", "--noali", hmmdb, query_filename])
    except:
        # if we successfully made it, we clean it up
        if query_filename is not None:
            os.unlink(query_filename)
        raise
    os.unlink(query_filename)
    # get the first result line (e.g. ">> ser_A1")
    for line in out.split("\n"):
        line = line.strip()
        if line and line[0] == '>':
            return line.split()[1].rsplit("_", 1)[0] # ">> ser_A1" -> "ser"
    return "no_call"

if __name__ == "__main__":
    main(sys.argv[1])
