# set fileencoding: utf-8
# License: GNU Affero General Public License v3 or later
# A copy of GNU AGPL v3 should have been included in this software package in LICENSE.txt.

from __future__ import print_function, division

from Bio import Phylo
from StringIO import StringIO

class Leaf(object):
    __slots__ = ["num", "id", "spec", "node"]
    def __init__(self, num, name, spec, node):
        # type: (int, str, str, Phylo.Clade) -> None
        self.num = num
        self.id = str(name)
        self.spec = str(spec)
        self.node = node

    def __repr__(self):
        return "Leaf({}, {}, {}, {})".format(self.num, self.id, self.spec, repr(self.node))

## set wildcard for query seqs in the tree
WILD = 'UNK'
ZERO_THRESH = 0.005

## Set normalization pair
NPPREF = ('Q70AZ7_A3', 'Q8KLL5_A3')

## Set normalized distance cutoff for nearest neighbor
## (empirically derived default = 2.5)
DCUT = 2.5

def get_parent(tree, child_clade):
    # type: (Phylo.BaseTree, str) -> Phylo.Clade
    node_path = tree.get_path(child_clade)
    if len(node_path) < 2:
        return None
    return node_path[-2]

def calcscore(scaleto, distance):
    # type: (float, float) -> float
    if distance >= scaleto:
        return 0.
    return (scaleto - distance) / scaleto

def getscore(scaleto, nd, dist2q, leaves, o):
    # type: (float, float, Dict[int, int], Dict[int, Leaf], List[int]) -> float
    """
        scaleto:
        nd: normalisation distance
        dist2q: leaf num -> distance mapping
        leaves: leaf num -> Leaf mapping
        o: leaf nums in order of increasing distance
    """
    score = 0.
    nnspec = leaves[o[0]].spec
    for n in o:
        if nnspec != leaves[n].spec:
            break
        tmpscore = calcscore(scaleto, dist2q[n] / nd)
        if tmpscore <= 0:
            break
        score += tmpscore
    return score

def siblings_by_distance(tree, parent, query_leaf):
    # type: (Phylo.Tree, Phylo.Clade, Leaf) -> List[str]
    """ returns a list of sibling leaves,
        ordered by increasing distance from the query leaf"""
    dists = {} # type: Dict[str, Any]
    for sibling in parent.get_terminals():
        dists[sibling] = tree.distance(query_leaf.node, sibling)
    return sorted(dists, key=dists.get)

def deeperdive(query, tree, n, p, leaves):
    # type: (int, Phylo.BaseTree, int, int, Dict[int, Leaf]) -> Tuple[str, str, str]
    """ returns spec prediction, hit id, forced or not """
    query_leaf = leaves[query]
    leaf_n = leaves[n]
    leaf_p = leaves[p]

    ## Want q to nearest dist to be less than nearest to pannearest dist
    q_to_n = tree.distance(query_leaf.node, leaf_n.node)
    n_to_pn = tree.distance(leaf_n.node, leaf_p.node)
    q_to_pn = tree.distance(query_leaf.node, leaf_p.node)
    if q_to_n < n_to_pn and leaf_n.spec == leaf_p.spec:
        return (leaf_n.spec, leaf_n.id, 'no_force_needed')
    if q_to_n == q_to_pn and leaf_n.spec != leaf_p.spec:
        return ('no_confident_result', 'N/A', 'no_confident_result')
    parent = get_parent(tree, query_leaf.node)
    if parent is None:
        return ('no_confident_result', 'N/A', 'no_confident_result')
    for node in siblings_by_distance(tree, parent, query_leaf):
        if node.name != query_leaf.id:
            return ('no_confident_result', 'N/A', node.name.split("_")[-1])
    return ('no_confident_result', 'N/A', 'no_confident_result')

def checkclade(query, lo, hi, wildcard, tree, leaves):
    # type: (int, int, int, str, Phylo.BaseTree, Dict[int, Leaf]) -> Tuple[str, str]
    if lo not in leaves or hi not in leaves:
        # we've walked out of the tree without finding a known leaf to compare to
        return ('deeperdive', 'N/A')
    if leaves[lo].spec == wildcard: ## lower bound is wildcard
        return checkclade(query, lo - 1, hi, wildcard, tree, leaves)
    if leaves[hi].spec == wildcard: ## upper bound is wildcard
        return checkclade(query, lo, hi + 1, wildcard, tree, leaves)

    ## Get the lca's descendants and check specs
    lca = tree.common_ancestor(leaves[lo].node, leaves[hi].node)
    spec = 'N/A'
    iname = 'N/A'
    for child in siblings_by_distance(tree, lca, leaves[query]):
        child_name, child_spec = child.name.rsplit("_", 1)
        # skip queries
        if child_spec == wildcard:
            continue
        # if they disagree on spec, do a deeperdive
        if spec != 'N/A' and child_spec != spec:
            return ('deeperdive', 'N/A')
        # once set, spec is final, iname will be from the closest sibling
        if spec == 'N/A':
            spec = child_spec
            iname = child_name

    return (spec, iname)


def build_leaf_info(tree):
    # type: (Phylo.Tree) -> Tuple[Dict[int, Leaf], List[int], List[int], float]
    query = []
    known = []
    leaves = {}
    npnode = ['', ''] # normalisation pair info

    ## Loop through leaves to find groups
    leafnum = 1

    for leaf in tree.get_terminals():
        if leaf.name.startswith(NPPREF[0]):
            npnode[0] = leaf
        elif leaf.name.startswith(NPPREF[1]):
            npnode[1] = leaf
        spec = leaf.name.rsplit("_", 1)[-1]
        leaves[leafnum] = Leaf(leafnum, leaf.name, spec, leaf)
        ## Record queries
        if spec == WILD:
            query.append(leafnum)
        else:
            known.append(leafnum)
        leafnum += 1
    if not npnode[0] or not npnode[1]:
        raise ValueError("Tree missing normalisation keys: %s, %s" % NPPREF)
    normalisation_distance = tree.distance(npnode[0], npnode[1])
    # avoid division by zero later on
    if normalisation_distance < 1e-8:
        normalisation_distance = 1.
    return leaves, query, known, normalisation_distance


def process_raw_tree(tree_data):
    # type: (str) -> Phylo.Tree
    if tree_data is None or not isinstance(tree_data, basestring):
        raise TypeError("Tree data must be a string stype")
    if not tree_data:
        raise ValueError("Invalid tree: %r" % tree_data)
    handle = StringIO(tree_data)
    ## Read tree
    try:
        tree = Phylo.read(handle, 'newick')
    except Phylo.NewickIO.NewickError as err:
        raise ValueError(err)
    return tree


def make_prediction(query, tree, leaves, known, normdist):
    # type: (int, Phylo.Tree, Dict[int, Leaf], List[int], float) -> List[str]
    forcedpred = 'no_force_needed'
    pred = 'no_pred_made'
    hit = 'N/A'
    ## Get distances to knowns
    distfromq = {}
    for n in known:
        distance = tree.distance(leaves[query].node, leaves[n].node)
        if distance >= 0:
            distfromq[n] = distance
        assert n != query
    # Sort distances
    ordered = sorted(distfromq, key=distfromq.get)
    ## Get near-zero distances
    z = []
    for n in ordered:
        distance = distfromq[n]
        if distance > ZERO_THRESH:
            break
        z.append(n)
    if len(z) > 0: ## query has zero-distance known neighbors
        leaf = leaves[z[0]]
        pred = leaf.spec
        hit = leaf.id.rsplit("_", 1)[0]
    else:
        ## check it out
        pred, hit = checkclade(query, query - 1, query + 1, WILD, tree, leaves)
        if pred == 'deeperdive':
            pred, hit, forcedpred = deeperdive(query, tree, ordered[0],
                                               ordered[1], leaves)
            if hit != 'N/A':
                hit = hit.rsplit("_", 1)[0]
    nn = float(distfromq[ordered[0]]) / normdist
    nnscore = 0.
    snnscore = 0.
    if nn < DCUT:
        snnscore = getscore(DCUT, normdist, distfromq, leaves, ordered)
        nnscore = calcscore(DCUT, nn)
    assert pred
    assert hit
    assert forcedpred
    return [leaves[query].id, pred, hit, forcedpred,
            str(nn), str(nnscore), str(snnscore)]


def main(tree_data):
    # type: (str) -> Optional[List[List[str]]]
    tree = process_raw_tree(tree_data)
    leaves, query, known, normdist = build_leaf_info(tree)

    if not query:
        raise ValueError("Query not contained by tree")
    return [make_prediction(q, tree, leaves, known, normdist) for q in query]

if __name__ == "__main__":
    import sys
    print(main(open(sys.argv[1]).read()))

