#!/bin/env python
# set fileencoding: utf-8
# License: GNU Affero General Public License v3 or later
# A copy of GNU AGPL v3 should have been included in this software package in LICENSE.txt.

from __future__ import print_function, division

import logging
import os
import sys
from collections import defaultdict

sandpumadir = __file__.rsplit("/", 2)[0]
for f in ["nodemap.tsv", "fullset0_smiles.faa", "unreliable_paths.txt"]:
    path = os.path.join(sandpumadir, "flat", f)
    if not os.path.exists(path):
        raise ImportError("sandpuma rescore module missing data file: %s"%f)

class Node(object):
    def __init__(self, line):
        # type: (str) -> None
        i, parent, parent_call, dec, thresh = line.split("\t")
        self.index = int(i)
        self.parent = None # type: Optional[Node]
        self.decision = None # type: str
        self.spec = None # type: str
        self.path = [] # type: List[Node]
        self.children = [] # type: List[Node]
        if parent == "NA":
            self.parent_index = None
            self.parent_call = None
        else:
            self.parent_index = int(parent)
            self.parent_call = parent_call == "T"
        self.leaf_node = dec == "LEAF_NODE"
        if not self.leaf_node:
            self.threshold = float(thresh)
            if dec == "pid":
                self.decision = dec
            else:
                decision, spec = dec.rsplit("_", 1)
                self.decision = cleannc(decision)
                if not self.decision:
                    raise ValueError("invalid decision value: %s", dec)
                self.spec = spec

    def link_child(self, child):
        # type: (Node) -> None
        self.children.append(child)
        assert child.parent is None
        child.parent = self

    def path_from_root(self):
        # type: () -> List[Node]
        if not self.path:
            if not self.parent:
                self.path = [self]
            else:
                self.path = self.parent.path_from_root()
            if not self.leaf_node:
                self.path += [self]
        return self.path

    def as_path_string(self):
        # type: () -> str
        """ creates the old-style string for this node
        """
        if self.leaf_node:
            return "LEAF_NODE-{}".format(self.index)
        # yes, they use the parent decision and threshold, not a bug here
        decision = self.parent.decision
        threshold = str(self.parent.threshold)
        call = "F"
        if self.parent_call is None:
            call = "NA"
        elif self.parent_call:
            call = "T"
        return "{}%{}-{}".format(decision, threshold, call)

    @staticmethod
    def path_as_string(nodes):
        # type: (List[Node]) -> str
        """ Generates a single string for the full path given in `nodes`
            only for creating an older style path string to recreate
            unreliable_paths.txt and similar
        """
        result = []
        for node in nodes[1:]:
            result.append(node.as_path_string())
        return "&".join(result)

class Query(object):
    def __init__(self, pid):
        self.pid = float(pid)
        self.path = None # type: Optional[List[Node]]
        self.methods = {} # type: Dict[str, str]


def build_nodes():
    # type: () -> List[Node]
    """ Reads in decision tree node map """
    nodefile = open(os.path.join(sandpumadir, "flat", "nodemap.tsv"), "r")
    nodes = {}
    for line in nodefile:
        if line[0] == "#":
            continue
        node = Node(line.strip())
        nodes[node.index] = node
    nodefile.close()

    # link nodes
    for node in nodes.values():
        if node.parent_index is not None:
            nodes[node.parent_index].link_child(node)

    if len(nodes.values()) > 1:
        for node in nodes.values():
            assert node.parent or node.children

    # find leaves
    leaves = [node for node in nodes.values() if not node.children]

    return leaves

def cleannc(s):
    s = s.replace("_result", "")
    for change in ["no_confident", "N/A", "no_call"]:
        s = s.replace(change, "nocall")
    return s


def build_m2p():
    m2p = {}
    cmfh = open(os.path.join(sandpumadir, "flat", "fullset0_smiles.faa"), "r")
    for line in cmfh:
        line = line.strip()
        if line[0] == '>':
            a = line[1:].split('_')
            q = "_".join(a[:-1]).replace(")", "-").replace("(", "-")
            m2p[q] = a[-1]
    cmfh.close()
    return m2p

def build_queries(pid_filename, ind_filename):
    # type: (str, str) -> Dict[str, Query]
    ## Parse pid results
    queries = {} # type: Dict[str, Query]
    with open(pid_filename, "r") as pid_file:
        for line in pid_file:
            line = line.strip()
            query, pid = line.split("\t")
            queries[query] = Query(pid)#{"pid":float(pid)}
    fill_in_ind_details(queries, ind_filename)

    leaves = build_nodes()

    ## Loop through all results
    for q in sorted(queries.keys()):
        for leaf in leaves:
            if leaf_passes(queries[q], leaf):
                queries[q].path = leaf.path_from_root()

    return queries

def fill_in_ind_details(queries, ind_filename):
    # type: (Dict[str, Query], str) -> None
    """ adds details to the queries dict
    """
    m2p = build_m2p()
    ## Parse individual results
    with open(ind_filename, "r") as results_file:
        for line in results_file:
            # ignore SNN scores
            try:
                query, method, called_spec = line.strip().split("\t", 3)[:3]
            except ValueError:
                logging.error("bad independent result: %s", line)
                raise
            if method == "pHMM":
                called_spec = called_spec.replace("-", "|")
            elif method == "prediCAT_MP":
                called_spec = m2p.get(called_spec, "nocall")
            elif method == "prediCAT_SNN" and called_spec == "no_force_needed":
                # if the files order ever has SNN before MP, this will break
                called_spec = queries[query].methods["prediCAT_MP"]

            queries[query].methods[method] = cleannc(called_spec)

def leaf_passes(query, leaf):
    # type: (Query, Node) -> bool
    for node in leaf.path_from_root():
        if node.decision == "pid":
            condition = node.threshold <= query.pid
        else:
            condition = query.methods[node.decision] == node.spec
        if node.parent_call == condition:
            return False
        # the other cases are fine
    return True

def read_unreliable_paths():
    # type: () -> Set[int]
    """ Reads the unreliable paths from fixed file, only uses leaf ID to mark
        an unreliable path
        """
    unreliable = set()
    with open(sandpumadir + "/flat/unreliable_paths.txt", "r") as ref:
        for line in ref:
            unreliable.add(int(line.strip().rsplit("LEAF_NODE-", 1)[1]))
    return unreliable

def main(pid_filename, ind_filename, ens_filename):
    # type: (str, str, str) -> List[str]
    queries = build_queries(pid_filename, ind_filename)

    unreliable = read_unreliable_paths()

    ensemble_file = open(ens_filename, "r")
    return_val = []
    for line in ensemble_file:
        query_name, _, spec = line.strip().split("\t")
        query = queries[query_name]
        if query.pid >= 97: # almost perfect match
            counts = defaultdict(lambda: 0) # type: DefaultDict[str, int]
            for specs in query.methods.values():
                if specs == "nocall":
                    continue
                for classification in specs.split("|"):
                    counts[classification.strip()] += 1
            if counts:
                sorted_counts = [i[0] for i in sorted(counts.iteritems(), reverse=True,
                                                      key=lambda x: x[1])]
                highscore = counts[sorted_counts[0]]
                called_spec = [sorted_counts[0]]
                # add any specs with tied scores
                for count in sorted_counts[1:]:
                    if counts[count] < highscore:
                        break
                    called_spec.append(count)
            else:
                called_spec = ['no_call']
        elif query.path and query.path[-1].index in unreliable: # leaf of path
            ## Report no call for unreliable path
            called_spec = ['no_call']
        else:
            called_spec = [spec]

        return_val.append("%s\tSANDPUMA\t%s"%(query_name, "|".join(called_spec)))

    ensemble_file.close()
    return return_val


if __name__ == "__main__":
    for res in main(sys.argv[1], sys.argv[2], sys.argv[3]):
        print(res)
