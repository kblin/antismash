# set fileencoding: utf-8
# License: GNU Affero General Public License v3 or later
# A copy of GNU AGPL v3 should have been included in this software package in LICENSE.txt.

from __future__ import print_function, division

import os
import unittest
from minimock import mock
from tempfile import NamedTemporaryFile

import antismash.specific_modules.nrpspks.sandpuma.bin.allpred_nodep_par as allpred_nodep_par
import antismash.specific_modules.nrpspks.sandpuma.bin.classifyp as classifyp
import antismash.specific_modules.nrpspks.sandpuma.bin.nrpscodepred as nrpscodepred
import antismash.specific_modules.nrpspks.sandpuma.bin.rescore as rescore
import antismash.specific_modules.nrpspks.sandpuma.bin.treeparserscore as treeparserscore

class TestRescore(unittest.TestCase):
    def test_Y16952(self):
        self.run_full("Y16952")

    def run_full(self, subdir):
        data_dir = os.path.join(os.path.dirname(__file__), "rescore_files", subdir)
        independent = os.path.join(data_dir, "ctg1_ind.res.tsv")
        ensemble = os.path.join(data_dir, "ctg1_ens.res.tsv")
        pid = os.path.join(data_dir, "ctg1_pid.res.tsv")
        result = os.path.join(data_dir, "ctg1_sandpuma.tsv")

        with open(result, "r") as result_file:
            expected = [line.strip() for line in result_file]
        results = rescore.main(pid, independent, ensemble)
        self.assertEqual(results, expected)

class TestTreeParserScore(unittest.TestCase):
    def compare(self, data, expected):
        results = treeparserscore.main(data)
        self.assertEqual(expected, results)

    def test_bad_input(self):
        for bad in [None, 1, 0.5]:
            with self.assertRaises(TypeError) as context:
                treeparserscore.main(bad)
        for bad in ["", "("]:
            with self.assertRaises(ValueError) as context:
                treeparserscore.main(bad)
        with self.assertRaises(ValueError) as context:
            treeparserscore.main("A")
        self.assertTrue(str(context.exception).startswith("Tree missing normalisation keys"))
        
        # no queries
        with self.assertRaises(ValueError) as context:
            treeparserscore.main('(Q70AZ7_A3_specA:0.2,Q8KLL5_A3_specB:0.3)')
        self.assertEqual("Query not contained by tree", str(context.exception))

    def test_artificial_input(self):
        # ensure no zero division errors occur
        treeparserscore.main('Q70AZ7_A3_specA,Q8KLL5_A3_specB,test_UNK')

        # differing specs and distances
        self.compare('(Q70AZ7_A3_specA:0.15,Q8KLL5_A3_specA:0.1),test_UNK:0.01',
                     [['test_UNK', 'specA', 'Q8KLL5_A3', 'no_force_needed', '0.44', '0.824', '1.568']])

        # equidistant and differing specs
        self.compare('(test_UNK:0.2,(Q70AZ7_A3_specA:0.2,Q8KLL5_A3_specB:0.2))',
                     [['test_UNK', 'no_confident_result', 'N/A', 'no_confident_result', '1.0', '0.6', '0.6']])

        # query with no parent (requires varied distances and same specs)
        self.compare('test_UNK:0.2,(Q70AZ7_A3_specA:0.2,Q8KLL5_A3_specB:0.3)',
                     [['test_UNK', 'no_confident_result', 'N/A', 'no_confident_result', '0.8', '0.68', '0.68']])

        # has parent, has siblings
        self.compare('Q70AZ7_A3_specA:0.15,(Q8KLL5_A3_specA:0.1,test_UNK:0.01,other_specC:0.05)',
                     [['test_UNK', 'no_confident_result', 'N/A', 'specC', '0.24', '0.904', '0.904']])

        # query with parent and no siblings (requires varied distances and same specs)
        self.compare('other_specC:1.5,(Q70AZ7_A3_specA:0.2,Q8KLL5_A3_specB:0.3,(test_UNK:0.2))',
                     [['test_UNK', 'no_confident_result', 'N/A', 'no_confident_result', '0.8', '0.68', '0.68']])

        # distance greater than cutoff
        self.compare('other_specA:150,(Q70AZ7_A3_specA:1,Q8KLL5_A3_specA:1,(test_UNK:0.1))',
                     [['test_UNK', 'specA', 'Q70AZ7_A3', 'no_force_needed', '0.55', '0.78', '1.56']])

class TestExecute(unittest.TestCase):
    def test_execute_simple(self):
        out = allpred_nodep_par.execute(["echo", "test"])
        self.assertEqual(out, "test\n", "simple output generation failed")

    def test_execute_output(self):
        try:
            allpred_nodep_par.execute(["cat", "/dev/null"])
            self.fail("test command should raise an exception, but didn't")
        except RuntimeError as e:
            self.assertTrue(str(e).startswith("cat failed to generate output"))

    def test_execute_exit_value(self):
        try:
            allpred_nodep_par.execute(["cat", "/a/path/that/doesnt/exist"])
            self.fail("test command should raise an exception, but didn't")
        except RuntimeError as e:
            self.assertTrue(str(e).startswith("cat exited with non-zero value"))

class TestHelpers(unittest.TestCase):
    def test_fasta_reader(self):
        res = allpred_nodep_par.read_fasta([">a", "ABC", ">b", "DEF"])
        self.assertEqual(len(res), 2)
        self.assertEqual(res["a"], "ABC")
        self.assertEqual(res["b"], "DEF")
        res = allpred_nodep_par.read_fasta([">a", "ABC", "DEF"])
        self.assertEqual(len(res), 1)
        self.assertEqual(res["a"], "ABCDEF")
        with NamedTemporaryFile() as io_test:
            io_test.write("\n".join([">a", "ABC", ">b", "DEF"]))
            io_test.flush()
            io_test.seek(0)
            res = allpred_nodep_par.read_fasta(io_test)
            self.assertEqual(len(res), 2)
            self.assertEqual(res["a"], "ABC")
            self.assertEqual(res["b"], "DEF")

    def test_fasta_reader_invalid_input(self):
        # bad lines
        for input_lines in [[">a"], [">a", "ABC", ">b"], ["DEF"]]:
            with self.assertRaises(ValueError):
                allpred_nodep_par.read_fasta(input_lines)

    def test_concatenate_file(self):
        # helper functions for iteration
        def get_name(target):
            return target.name
        def get_handle(target):
            return target.file
        lines = ["a\n", "b\n", "c\n"]
        # test empty file, single line, and multiline
        for i in range(len(lines)):
            # and all 4 combos of filename and file handle
            for func_target in [get_name, get_handle]:
                for func_source in [get_name, get_handle]:
                    target = NamedTemporaryFile()
                    source = NamedTemporaryFile()
                    target.writelines(lines[:i])
                    source.writelines(lines[i:])
                    target.flush()
                    source.flush()
                    source.seek(0)
                    allpred_nodep_par.concatenate_file(func_source(source), func_target(target))
                    # if we passed a handler, have to ensure data is
                    # flushed before we check it's contents
                    target.flush()
                    with open(target.name, "r") as handle:
                        self.assertEqual(lines, handle.readlines())
                    target.close()
                    source.close()

class TestMethods(unittest.TestCase):
    def test_ASM(self):
        for bad_query in [None, {}]:
            res = allpred_nodep_par.code2spec(os.devnull, bad_query)
            self.assertEqual(res, "no_call")
        for bad_query in [7, {"A":"ABC"}, ["ABC"]]:
            with self.assertRaises(TypeError):
                allpred_nodep_par.code2spec(os.devnull, bad_query)
        with self.assertRaises(AssertionError):
            allpred_nodep_par.code2spec(os.devnull, "TOOFEW")
        reference_file = os.path.join(os.path.dirname(os.path.dirname(__file__)),
                                      "flat", "fullset0_smiles.stach.faa")
        # test dummy code and real codes from Y16952
        for code, expected in [("ABCABCABC", "no_call"),
                               ("DAFYLGMMC", "leu"),
                               ("DTSKVAAIC", "bht|tyr"),
                               ("DLTKLGEVG", "asn"),
                               ("DIFHLGLLC", "hpg|hpg2cl"),
                               ("DAVHLGLLC", "hpg|hpg2cl|tyr"),
                               ("DASTLGAIC", "bht|tyr"),
                               ("DPYHGGTLC", "dpg"),
                               ("DILQCGMVW", "gly"),
                               ("DAATLAAVA", "bht|tyr")]:
            result = allpred_nodep_par.code2spec(reference_file, code)
            self.assertEqual(result, expected, "%s -> %s, expected %s" % (code, result, expected))

    def test_stachextract(self):
        seq_id = 'bpsA_A2_UNK'
        seq = """SYADLWERSLKFAAVLRAHGVRSEDRVGLVVGRSAWWTVGMLGVLLAGGTFVP
                 VDPAYPAERKEWIFRSANPMLVVCAGATRGAVPAEFADRLVVIDEVDPAAGSAGDLPRVDPR
                 SAAYVIYTSGSTGTPKGVVVTHAGLGNLALAHIDRFGVSPSSRVLQFAALGFDTIVSEVMMA
                 LLSGATLVVPPERDLPPRASFTDALERWDITHVKAPPSVLGTADVLPSTVETVVAAGELCPP
                 GLVDRLSADRRMINAYGPTETTICATMSMPLSPGQHPIPFGKPVPGVRGYLLDSFLRPLPPG
                 VTGELYLAGIGVARGYLGRSALTAERFVADPFVPGERMYRTGDLAYWTEQGELVSAGRADDQ
                 VKIRGFRVEPREIEFALSGYPRVTQAAVAVRDDRLVAYVTPGDIDTQAVRAHLASRMPQYMV
                 PAAVVALDALPLTAHGKIDRRALPDPDFTAGKQAREPATETERVLCELFAGVLGLARVGVDD
                 SFFELGGDSILSMQLAARARRSGL""".replace(" ", "").replace("\n", "")
        basedir = os.path.dirname(os.path.dirname(__file__))
        result = allpred_nodep_par.stachextract((seq_id, seq), basedir)
        self.assertEqual(result, 'DTSKVAAIC')


class TestClassify(unittest.TestCase):
    def test_invalid_arg_types(self):
        classify = classifyp.main
        with self.assertRaises(TypeError) as context:
            classify([], max_depth="bad_depth")
        self.assertEqual("max_depth is required to be an int or None", str(context.exception))
        with self.assertRaises(TypeError) as context:
            classify([], msl=None)
        self.assertEqual("msl is required to be an int", str(context.exception))
        with self.assertRaises(TypeError) as context:
            classify([], msl="1")
        self.assertEqual("msl is required to be an int", str(context.exception))

    def test_invalid_arg_values(self):
        classify = classifyp.main
        labels = ["75", "144"]
        features = [["56.73", "1", "0"], ["66.21", "0", "1"]]
        with self.assertRaises(ValueError) as context:
            classify([], features=features[1:], labels=labels)
        self.assertTrue(str(context.exception).startswith("feature length"))
        self.assertTrue(str(context.exception).endswith("mismatch"))
        with self.assertRaises(ValueError) as context:
            classify([], features=features, labels=labels[1:])
        self.assertTrue(str(context.exception).startswith("feature length"))
        self.assertTrue(str(context.exception).endswith("mismatch"))

    def test_simple_classification(self):
        features = [[0, 0], [1, 1]]
        labels = [0, 1]
        matrix = [2, 2]
        res = classifyp.main(matrix, features=features, labels=labels)
        self.assertEqual(res, 1)

    def test_label_reading(self):
        temp = NamedTemporaryFile()
        data = [str(i) for i in range(10)]
        temp.write("\n".join(data))
        temp.flush()
        temp.seek(0)
        labels = classifyp._read_labels(temp.name)
        temp.close()
        self.assertEqual(labels, data)

    def test_feature_reading(self):
        temp = NamedTemporaryFile()
        data = [[str(float(i)), str(i*10), str(i*100)] for i in range(10)]
        temp.write("\n".join(["%s\t%s\t%s" % (i, j, k) for i, j, k in data]))
        temp.flush()
        temp.seek(0)
        labels = classifyp._read_features(temp.name)
        temp.close()
        self.assertEqual(labels, data)

class TestNRPSCodePred(unittest.TestCase):
    def setUp(self):
        self.file = None

    def tearDown(self):
        pass

    def test_output(self):
        #not in setUp because we need functional files for nose and other tests
        self.file = NamedTemporaryFile()

        # test a valid input
        name, seq = "bpsA_A1_UNK", "L--SFDASLFEMYLLTGGDRNMYGPTEATMCATW"
        nrpscodepred.output(seq, name, self.file.name)
        self.assertEqual(self.file.read(), "{}\t{}\n".format(seq, name))
        # test combinations of bad inputs
        inputs = ["str", 7, None, self]
        for i in inputs:
            for j in inputs[1:]: #skip two strings, because that
                with self.assertRaises(TypeError) as context:
                    nrpscodepred.output(i, j, self.file.name)
                self.assertEqual("Query sequence id and sequence must be strings",
                                 str(context.exception))
        self.file.close()

    def test_clean_sequence(self):
        pre = nrpscodepred.ILLEGAL_CHARS[:]
        cleaned = nrpscodepred.clean_seq(pre)
        self.assertEqual(len(pre), len(cleaned))
        self.assertEqual(cleaned, "X"*len(pre))

        pre = "".join([chr(i) for i in range(ord('A'), ord('Z') + 1)])
        cleaned = nrpscodepred.clean_seq(pre)
        self.assertEqual(pre, cleaned)

        #grab your ascii table
        pre = [chr(i) for i in range(33, 65)] # ! to @
        pre.extend([chr(i) for i in range(91, 97)]) # [ to '
        pre.extend([chr(i) for i in range(123, 127)]) # { to -
        pre = "".join(pre)
        cleaned = nrpscodepred.clean_seq(pre)
        self.assertEqual(len(pre), len(cleaned))
        self.assertEqual(cleaned, "X"*len(pre))

    def test_read_positions(self):
        # this test is a little silly, it's only purpose is to detect change
        positions = nrpscodepred.read_positions(nrpscodepred.APOSITION_FILENAME,
                                                nrpscodepred.START_POSITION)
        self.assertEqual(positions, [169, 170, 173, 212, 233, 235, 256, 264, 265])

        positions = nrpscodepred.read_positions(nrpscodepred.A34_POSITIONS_FILENAME,
                                                nrpscodepred.START_POSITION)
        self.assertEqual(positions, [144, 147, 148, 164, 168, 169, 170, 171,
                                     172, 173, 174, 177, 212, 213, 233, 234,
                                     235, 236, 237, 254, 255, 256, 257, 258,
                                     259, 260, 261, 262, 263, 264, 265, 266,
                                     267, 268])

    def test_build_position_list(self):
        # trivial case
        positions = range(10)
        sequence = ["A"]*10
        result = nrpscodepred.build_position_list(positions, sequence)
        self.assertEqual(positions, result)

        # more complicated
        for length in range(25, 200, 50):
            for start in range(0, 2):
                positions = range(start, length, 2)
                sequence = ["-"]*length
                # sets sequence for all positions to be - if start == 0
                for i in positions:
                    sequence[i + start] = "A"
                sequence = "".join(sequence)
                result = nrpscodepred.build_position_list(positions, sequence)
                if not start:
                    self.assertEqual(result, positions[::2])
                else:
                    self.assertEqual(result, range(4, length+1, 4))

    def test_extract(self):
        # trivial case with no gaps
        positions = range(0, 10)
        sequence = "".join([chr(i) for i in range(ord('A'), ord('A') + 10)])
        for i in range(1, 5):
            result = nrpscodepred.extract(sequence, positions[::i])
            self.assertEqual(result, sequence[::i])
        # with gaps
        sequence = "A-CD"
        positions = [0, 1, 2]
        self.assertEqual(nrpscodepred.extract(sequence, positions), "A-C")
        positions = [1, 2]
        self.assertEqual(nrpscodepred.extract(sequence, positions), "AC")
        positions = [0, 1]
        self.assertEqual(nrpscodepred.extract(sequence, positions), "AC")
        positions = range(len(sequence))
        self.assertEqual(nrpscodepred.extract(sequence, positions), sequence)
        positions = [0, 3]
        self.assertEqual(nrpscodepred.extract(sequence, positions), "AD")

    def test_total(self):
        mock("nrpscodepred.run_muscle", returns={
            "P0C062_A1" : "TYHELNVKANQLARIFIEK---GI--GKDTLVGIMMEKSIDLFIGILAVLKA"
                          "GGAYVPID--I-----EYPKERIQYILDD-SQARMLL--TQ-------KHL-"
                          "----------------------------------------------------"
                          "----------------------VHLI----HNIQFNG------Q------VE"
                          "IF---------------------------------EED---------TI---"
                          "-KIREGTN----------LHV----P----------------------SKST"
                          "D--LAYVIYTSGTT----GNPKGTMLEHKGISNLKV--F--F-----ENSL-"
                          "NV----TE-KD-------RIGQFASIS--FDAS-VWEMFMALLTGASLYII-"
                          "LK-DTINDFVKFEQYINQKE-ITVITLPPTYVVHLD-PER------------"
                          "ILS-----------------IQ--TLITA--GSATSPS-LV---NKWKEK--"
                          "-----VT-----YINAYGPTETTICATTWV----------ATK---ETT---"
                          "-------------GHSVPI-GAPI-QNTQIYIV----DENLQLK-SVGE-AG"
                          "ELCIGG-EGLARGYWKRPELTS---QKFVD-NP---F-----V---P-----"
                          "---GE-----------------------------KLYKTGDQARWLP-D---"
                          "-G--NIEYLG-RID-NQVKIRGHRVELEEVESIL-L----KHMYISETAV--"
                          "----------------------------------------------------"
                          "----------------------------------------------------"
                          "--------------",
            "bpsA_A1_UNK" : "TYRQLDERAGRLAGRLASR---GI--RRGDRVAVVMDRSADLVVALLAVW"
                            "KAGAAYVPVD--A-----GYPAPRVAFMVAD-SAAKLVV--CS-------"
                            "AAS-----------------------------------------------"
                            "----------------------------RGAV--------PAG-------"
                            "------VESL---------------------------------EPA----"
                            "-----AA-----AEEGAS-----------DA----P--------------"
                            "AAT-----VRPGD--PAYVMYTSGST----GTPKGVTISQGCVAELTM--"
                            "---------DAGW-AM----EP-GE-------AVLMHSPHA--FDAS-LF"
                            "ELWMPLASGVRVVLA-EP--GSVDARRLREAAAAG--VTRVYLTAGSLRA"
                            "VA--EE--------APESFAE-----------------FR--EVLTG--G"
                            "DVVPAH-AV---ERVRTA--AP--RAR-----FRNMYGPTEATMCATWHL"
                            "----------LQP---GDV---------------VGPVVPI-GRPL-TGR"
                            "RVQVL----DASLRPV-GPGV-VGDLYLSG--ALAEGYFNRAALTA---E"
                            "RFVA-DP--------SA---P--------GQ-------------------"
                            "----------RMYWTGDLAQWTA-D----G--ELVFAG-RAD-DQVKIRG"
                            "FRIEPGEIEAAL-I----AQPDVHDAVVAAVDGRLIGYVVTEGDADPRVI"
                            "RERLGAVLPEHLVPAAVLALDALPLTGNGKVDRSALPAPEFAASAAGRAP"
                            "STDAERVLCGLFAEVLGVARAGVDDGFFELGGDSIGAMRLAARAAKAG"})
        query_id = "bpsA_A1_UNK"
        query_seq = ("TYRQLDERAGRLAGRLASRGIRRGDRVAVVMDRSADLVVALLAVWKAGAA"
                     "YVPVDAGYPAPRVAFMVADSAAKLVVCSAASRGAVPAGVESLEPAAAAEE"
                     "GASDAPAATVRPGDPAYVMYTSGSTGTPKGVTISQGCVAELTMDAGWAME"
                     "PGEAVLMHSPHAFDASLFELWMPLASGVRVVLAEPGSVDARRLREAAAAG"
                     "VTRVYLTAGSLRAVAEEAPESFAEFREVLTGGDVVPAHAVERVRTAAPRA"
                     "RFRNMYGPTEATMCATWHLLQPGDVVGPVVPIGRPLTGRRVQVLDASLRP"
                     "VGPGVVGDLYLSGALAEGYFNRAALTAERFVADPSAPGQRMYWTGDLAQW"
                     "TADGELVFAGRADDQVKIRGFRIEPGEIEAALIAQPDVHDAVVAAVDGRL"
                     "IGYVVTEGDADPRVIRERLGAVLPEHLVPAAVLALDALPLTGNGKVDRSA"
                     "LPAPEFAASAAGRAPSTDAERVLCGLFAEVLGVARAGVDDGFFELGGDSI"
                     "GAMRLAARAAKAG")
        expected = ("L--SFDASLFEMYLLTGGDRNMYGPTEATMCATW", "bpsA_A1_UNK")
        query = (query_id, query_seq)
        results = nrpscodepred.run_nrpscodepred(query)
        self.assertEqual(results, expected)
