# set fileencoding: utf-8
# License: GNU Affero General Public License v3 or later
# A copy of GNU AGPL v3 should have been included in this software package in LICENSE.txt.

from __future__ import print_function, division

import os
import unittest

import antismash.specific_modules.nrpspks.sandpuma.bin.treeparserscore as treeparserscore

class TestTreeparserscore(unittest.TestCase):
    def test_real_single_sequence(self):
        """ Test PrediCAT prediction for a single sequence in one tree """
        # a real A domain from Y16952, with tree generated as done in sandpuma
        expected = [["bpsC_A1_UNK", "dpg", "Q939Y9_A1", "no_force_needed",
                     "0.0", "1.0", "5.82202820099"]]
        with open(os.path.join(os.path.dirname(__file__), "fasttree_results.txt")) as datafile:
            data = datafile.read()
        self.assertEqual(expected, treeparserscore.main(data))

    def test_real_multi_sequence(self):
        """ Test PrediCAT predictions for multiple sequences in one tree """
        # real A domains from cluster 10 of NC_003888.3
        expected = [
            ['SCO3230_A1_UNK', 'ser', 'Q9Z4X6_A1', 'no_force_needed', '0.0', '1.0', '1.40124427854'],
            ['SCO3230_A2_UNK', 'athr|thr', 'Q9Z4X6_A2', 'no_force_needed', '0.0', '1.0', '1.0'],
            ['SCO3230_A3_UNK', 'trp', 'Q9Z4X6_A3', 'no_force_needed', '0.0', '1.0', '1.12767186597'],
            ['SCO3230_A4_UNK', 'asp', 'Q9Z4X6_A4', 'no_force_needed', '0.0', '1.0', '3.48652179709'],
            ['SCO3230_A5_UNK', 'asp', 'Q9Z4X6_A5', 'no_force_needed', '0.0', '1.0', '3.50404834911'],
            ['SCO3230_A6_UNK', 'hpg|hpg2cl', 'Q9Z4X6_A6', 'no_force_needed', '0.0', '1.0', '1.0'],
            ['SCO3231_A1_UNK', 'asp', 'Q9Z4X5_A1', 'no_force_needed', '0.0', '1.0', '3.62317913167'],
            ['SCO3231_A2_UNK', 'gly', 'Q9Z4X5_A2', 'no_force_needed', '0.0', '1.0', '2.0'],
            ['SCO3231_A3_UNK', 'asn', 'Q9Z4X5_A3-9-', 'no_force_needed', '0.0', '1.0', '2.0'],
            ['SCO3232_A1_UNK', 'glu|3-me-glu', 'Q8CJX2_A1-10-', 'no_force_needed', '0.0', '1.0', '1.0'],
            ['SCO3232_A2_UNK', 'trp', 'Q8CJX2_A2-11-', 'no_force_needed', '0.0', '1.0', '1.12767186597']]
        with open(os.path.join(os.path.dirname(__file__), "fasttree_results_multiple.txt")) as datafile:
            data = datafile.read()
        self.assertEqual(expected, sorted(treeparserscore.main(data)))

