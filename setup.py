import os
from setuptools import setup


def read_version():
    for line in open(os.path.join('antismash', '__init__.py'), 'r'):
        if line.startswith('__version__'):
            return line.split('=')[-1].strip().strip('"')


setup(
    name="antismash",
    version=read_version(),
    packages=['antismash', "."],
    entry_points='''
        [console_scripts]
        antismash=run_antismash:main
        download-antismash-databases=download_databases:main
    ''',
    include_package_data=True
)
