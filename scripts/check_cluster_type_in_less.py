#!/usr/bin/env python
"""Make sure custer rules and CSS is in sync."""
from __future__ import print_function


def main():
    """Check if all secmet classes from cluster_rules.txt have a CSS class in secmet.less."""
    defined_clusters = set()
    with open('antismash/generic_modules/hmm_detection/cluster_rules.txt', 'r') as handle:
        # skip first line
        handle.readline()
        for line in handle.readlines():
            cluster = line.split('\t')[0]
            defined_clusters.add(cluster.lower())

    available_classes = set()
    with open('antismash/output_modules/html/css/secmet.less', 'r') as handle:
        for line in handle.readlines():
            if not line.startswith('.'):
                continue
            class_ = line[1:].split()[0]
            available_classes.add(class_.lower())

    missing = defined_clusters - available_classes
    if missing:
        print("Missing classes:", sorted(list(missing)))
    else:
        print("All secmet classes have classes in secmet.less!")


if __name__ == '__main__':
    main()

